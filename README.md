

##Allegro Scrapper

A Python scrapper for www.allegro.pl site, made for learning purposes. Uses BeautifulSoup library
to scrape new deals based on users searched key words and chosen category

---

### Prerequisities

+ Installed python
+ Installed required python libs
```angular2html
pip install BeautifulSoup4
```

---

### Planned features

+ Add data-to-CSV export
+ Automate scrapper as a service for Arch Linux system
+ Add gui for notification about new offers with lowest prices