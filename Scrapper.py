from TypesDict import category_types, category2url
from requests import get
from urllib import parse
from bs4 import BeautifulSoup
from pprint import pprint
import json


class AllegroScrapper:
    def __init__(self):
        self.product_category = ""
        self.product_name = ""
        self.base_url = "https://allegro.pl"
        self.category_url = ""
        self.name_url = "string="
        self.extra_url = "&order=p&bmatch=baseline-ele-1-3-0725"

    def define_searched_product_category(self):
        self.product_category = input("Define what kind of product you are looking for. Type 'list' to see available "
                                      "types:\n")
        if self.product_category == 'list':
            self.show_types_list()
            self.define_searched_product_category()
        else:
            if self.product_category in category_types or \
                    any(self.product_category in values for values in category_types.values()):
                print("Category chosen: {}".format(self.product_category))
                self.define_searched_product_name()
            else:
                print("{} was not found".format(self.product_category))
                self.define_searched_product_category()

    @staticmethod
    def show_types_list():
        print("==== CATEGORIES LIST ====")
        for key in category_types.keys():
            print("+ {}".format(key))
            for value in category_types.get(key):
                print("\t - {}".format(value))

    def define_searched_product_name(self):
        self.product_name = input("Give the name of the product you are looking for:\n")
        if not self.product_name:
            print("Please, enter the name!")
            self.define_searched_product_name()

    def get_category_url(self):
        if self.product_category == 'Wszystko':
            return '/listing?'
        else:
            return '/kategoria/' + category2url.get(self.product_category) + '?'

    def get_data(self):
        product_url = parse.quote_plus(self.product_name)
        full_url = self.base_url + self.category_url + self.name_url + product_url + self.extra_url
        print(full_url)
        page = get(full_url)
        return page


def parse_data(page):
    found_offers = {
        "offers": [
        ]
    }

    soup = BeautifulSoup(page.text, 'html.parser')
    offers = soup.find_all('article')

    for offer in offers:
        spans = offer.find_all('span')
        link = offer.find('a', {'class': None})
        if link:
            my_title = link.get_text()
            my_href = link['href']
            if 'redirect=' in my_href:
                my_href = clear_redirected_html(my_href)
            for span in spans:
                text = span.get_text()
                if "zł" in text:
                    my_price = text
                    found_offers["offers"].append({"title": my_title, "price": my_price, "link": my_href})
                    break

    # TODO: Export do CSV i wyświetlanie najlepszej oferty (jako service)
    pprint(found_offers)


def clear_redirected_html(html):
    split_redirection = html.split("redirect=")
    split_html = split_redirection[1].split("?bi")
    return split_html[0]


if __name__ == "__main__":
    my_scrapper = AllegroScrapper()
    my_scrapper.define_searched_product_category()
    my_scrapper.category_url = my_scrapper.get_category_url()
    print("Searching for {name} in {category} category".format(name=my_scrapper.product_name, category=my_scrapper.product_category))
    page = my_scrapper.get_data()
    parse_data(page)



